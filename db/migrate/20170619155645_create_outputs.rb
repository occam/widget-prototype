class CreateOutputs < ActiveRecord::Migration[5.1]
  def change
	  create_table :outputs do |t|
		  t.integer :plotid
		  t.integer :oobject_id
		  t.string :objectid
	  end
  end
end
