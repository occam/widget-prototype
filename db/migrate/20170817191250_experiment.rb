class Experiment < ActiveRecord::Migration[5.1]
  def change
    create_table :experiments do |t|
	    t.string :friendly_name
	    t.string :workset_uuid
	    t.string :workset_revision
	    t.string :experiment_uuid
	    t.string :experiment_revision
    end
  end
end
