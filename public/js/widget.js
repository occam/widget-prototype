// The values for these globals should be build into the body tag.
var plot_count;
var widget_count;
var obj_pre;
var exp_id;
var occam_root;

// Initial Values.
var plot_index       = 1;
var result_iframe    = 2;
var hide_first_card  = false;
var hide_second_card = true;

// Helper Functions //////////////////////
// Return a string of the current widget ID.
function cur_index() {
  var path_split = window.location.pathname.split("/");
  return path_split[path_split.length-1];
}

// Return a string including the hostname, and port for building full paths.
function str_root() {
  return "http://" + location.hostname + ":" + location.port;
}

// Drag and Drop Functions //////////////////////
function drag_over(e) {
  e.stopPropagation();
  e.preventDefault();
  e.dataTransfer.dropEffect = 'copy';
}

// Adds the file dropped into the given frame.
var ALL_FILES = [];
function get_data(e, elm) {
  e.stopPropagation();
  e.preventDefault();
  ALL_FILES = $.merge($.merge([], e.dataTransfer.files), ALL_FILES);

  for (var i = 0, file; file = e.dataTransfer.files[i]; i++) {
    var name = file.name;
    document.getElementById(elm).innerText = name;
  }
}

// Occam Functions //////////////////////
// Returns the integer index of the first object in the workflow.
function get_wf_start_index(experiment) {
  var index = 0;
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: experiment + '/connectioninfo',
    crossDomain: true,
    cache: false,
    dataType: 'json',
    type: 'GET',
    async: false,
    success: function(data, textStatus, request) {
      index = data["max_index"];
    },
    error: function(data, textStatus, request) {
      console.log("Failed to determine experiment start index.");
    }
  });
  return index;
}

// Drops the first item on the experiment's workflow.
function drop_vol(experiment) {
  var new_url = "";
  var del_target = experiment + '/mod_connections/' + get_wf_start_index(experiment);
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: del_target,
    crossDomain: true,
    cache: false,
    type: 'DELETE',
    async: false,
    success: function(data, textStatus, request) {
      // Receive URL of new object.
      new_url = data;
      console.log("dropped vol: " + del_target);
    }
  });
  return new_url;
}

// Returns the route to the occam object(should be a volume) at the start of
// the workflow.
function obj_locate(experiment) {
  var post_target = '';

  console.log("experiment path is " + experiment);
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: experiment + '/connections/' + get_wf_start_index(experiment),
    crossDomain: true,
    cache: false,
    dataType: 'json',
    type: 'GET',
    async: false,
    success: function(data, textStatus, request) {
      if (data["type"] === "volume") {
        post_target = obj_pre + data["object"];
        console.log("post target is: " + post_target);
      }
      else {
        console.log("Something has gone wrong, the first object is not a volume.");
      }
    },
    error: function(data, textStatus, request) {
      console.log("Failed to determine post target"); 
    }
  });
  return post_target;
}

// Uploads to the given object and returns the new route.
function obj_upload(obj, formdata) {
  var new_url;
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: obj + '/tree/',
    crossDomain: true,
    data: formdata,
    cache: false,
    processData: false,
    contentType: false,
    type: 'POST',
    async: false,
    success: function(data, textStatus, request) {
      // Receive URL of new object. We split this because the url given
      // back is fully qualified, which the current route does not need.
      new_url = request.getResponseHeader('New_URL');
    }
  });
  return new_url;
}

// Attaches attach_vol to attach_target's workflow.
function attach_to_workflow(attach_target, attach_vol) {
  var index      = get_wf_start_index(attach_target);
  var attach_url =
    attach_target + "/mod_connections?object="
    + attach_vol + "&connection_index=" + index;

  var new_workflow;
  console.log("attach url: " + attach_url);
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: attach_url,
    crossDomain: true,
    cache: false,
    contentType: false,
    type: 'POST',
    async: false,
    success: function(data, textStatus, request) {
      // Receive URL of new object.
      new_workflow = data;
    },
    error: function(data, textStatus, request) {
      console.log("Failed to attach volume to workflow.");
    }
  });
  return new_workflow;
}

// Upload files stored in formdata into the given experiment's post_target.
function upload_to_vol(experiment, post_target, formdata) {
  var attach_vol = obj_upload(post_target, formdata).split('/')[5];
  var attach_target = occam_root + drop_vol(experiment);
  return attach_to_workflow(attach_target, attach_vol);
}

// Tell OCCAM to run the given experiment.
function queue_job(experiment) {
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: experiment + "/queue_job_remote",
    crossDomain: true,
    async: false,
    success: function(data, textStatus, request) {
      console.log("Queued a job.");
    },
    error: function(data, textStatus, request) {
      console.log("Job queue failed.");
    }
  });
}

// Asks OCCAM if the run has terminated.
function is_run_finished(experiment) {
  var results_in = false;
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: experiment + "/poll_til_done",
    crossDomain: true,
    async: false,
    success: function(data, textStatus, request) {
      console.log(request.status);
      console.log("Polled for refresh.");
      if (request.status != 204)
        results_in = true;
    },
    error: function(data, textStatus, request) {
      console.log(request.status);
      console.log("Poll failed.");
      results_in = true;
    }
  });

  return results_in;
}

// Animation Functions //////////////////////

// Start the loading animation
function spin_start() {
  console.log('start overlay');
  var spin_target = document.getElementById('overlay');
  spin_target.style.display = 'block';
}

// Stop the loading animation.
function spin_stop() {
  console.log('stop overlay');
  var spin_target = document.getElementById('overlay');
  spin_target.style.display = 'none';
}

// Flip between the visible and hidden divs.
function flip_card_1() {
  var x = document.getElementById('div1');
  x.classList.toggle("flip")
}

// Flip between the visible and hidden divs.
function flip_card_2() {
  var x = document.getElementById('div2');
  x.classList.toggle("flip")
}

function toggle_card_1() {
  var x = document.getElementById('div1');
  if (hide_first_card == false) {
    x.style.display = "none";
    hide_first_card = true;
  }
  else if (hide_first_card == true) {
    x.style.display = "block";
    hide_first_card = false;
  }
}

function toggle_card_2() {
  var x = document.getElementById('div2');
  if (hide_second_card == false) {
    x.style.display = "none";
    hide_second_card = true;
  }
  else if (hide_second_card == true) {
    x.style.display = "block";
    hide_second_card = false;
  }
}

// Plot Upkeep Functions //////////////////////

// Ask our own webserver to give us a new widget.
function request_new_widget(new_url) {
  var new_widget_url;
  $.ajax({
    url: new_url,
    cache: false,
    processData: false,
    contentType: false,
    type: 'GET',
    async: false,
    success: function(data, text_status, request) {
      new_widget_url = data;
    }
  });

  return new_widget_url;
}

// Unregisters plots from a given widget. This prevents doubling up on plots
// when constantly running the same widget.
function clear_registered_outputs(widget_url) {
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: widget_url.split("?")[0] + "/clear",
    type: 'POST',
    crossDomain: true,
    async: false
  });
}

// Register the outputs found as viewable plots.
function register_outputs(register_url, new_widget_url) {
  // Request the invocations data.
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: register_url,
    type: 'GET',
    dataType: 'json',
    crossDomain: true,
    async: false,
    success: function(data, textStatus, request) {
      // Go through the invocations and add all plotly files to the list of
      // registered outputs.
      for (var key in data) {
        for (var subindex in data[key]) {
          if (data[key][subindex]["object"]["subtype"] == 'plotly') {
            console.log("Registering " + data[key][subindex]["object"]["id"]);
            console.log(data[key][subindex]["object"]["subtype"]);

            // Register to the target widget.
            $.ajax({
              xhrFields: {
                withCredentials: true
              },
              url: new_widget_url + "/register?object=" + data[key][subindex]["object"]["id"],
              type: 'GET',
              async: false,
              error: function(data, textStatus, request) {
                console.log("Failed to register output.");
              }
            });
            plot_count++;
          }
        }
      }
      console.log("plot_count" + plot_count);
    },
    error: function(data, textStatus, request) {
      console.log("Failed to get the output manifest from OCCAM.");
    }
  });
}

// IFrame Manipulation Functions //////////////////////

// Loads the next plot in the next iframe and flips to it.
function load_next_plot(objectid, plot_index) {
  var resulturl = str_root() + "/results/" + objectid + "/output/"+plot_index;
  var iframe_elm;

  if (result_iframe == 2) {
    result_iframe = 1;
    iframe_elm    = document.getElementById('iframe1');
  } else {
    result_iframe = 2;
    iframe_elm    = document.getElementById('iframe2');
  }

  if (iframe_elm) {
    iframe_elm.setAttribute('src', resulturl);
    setTimeout(flip_card_2, 3000);
  }

  console.log("next plot");
}

// Load the next iframe with the next available plot.
function next_result() {
  var currenturl = window.location.href;
  var arrayurl   = currenturl.split("/");
  var objectid   = arrayurl[arrayurl.length-1].split("?")[0];

  if (plot_index < plot_count) {
    plot_index++;
    load_next_plot(objectid, plot_index);
  }
  else if (plot_index == plot_count && plot_index != 1) {
    plot_index = 1;
    load_next_plot(objectid, plot_index);
  }
}

// Switch hidden div to a different result page.
function display_result() {
  var currenturl = window.location.href;
  var arrayurl   = currenturl.split("/");
  var objectid   = arrayurl[arrayurl.length-1];
  var resulturl  = str_root() + "/results/" + objectid + "/output/1";
  var iframe_elm = document.getElementById('iframe2');
  if (iframe_elm)
    iframe_elm.setAttribute('src',resulturl);
}

// Core Event Functions //////////////////////

// Wait for the experiment to finish, then switch to the results. Should be
// 205(refresh) or 204(no updates).
function poll_loop(experiment, register_url, new_widget_url, file_count) {
  if (!is_run_finished(experiment)) {
    setTimeout(function() {poll_loop(experiment, register_url, new_widget_url, file_count);}, 5000);
  } else {
    // Only redirect if there is something to jump to.
    if (file_count > 0) {
      register_outputs(register_url, new_widget_url);
      new_widget_url = new_widget_url.replace(new RegExp("/widget/", "gm"), "/name/");
      window.location = new_widget_url + "?backside=";
    } else {
      clear_registered_outputs(window.location.href);
      register_outputs(register_url, new_widget_url);
    }

    display_result();
    spin_stop()
    flip_card_1();
    flip_card_2();
    setTimeout(toggle_card_1,400);
  }
}

// Upload all input changes to the Occam server and kick off the experiment.
function run_experiment() {
  var formdata = new FormData();
  var file_count = 0;

  for (var i = 0, file; file = ALL_FILES[i]; i++) {
    console.log("Found a file");
    file_count += 1;
    formdata.append('files[]', file);
  }

  // Build full paths for obj/exp/file posting.
  var object_path     = occam_root + document.body.getAttribute("data_post_target");
  var experiment_path = occam_root + document.body.getAttribute("data_fetch_target");

  // Build paths for running the experiment.
  var register_url = occam_root + "/objects/" + exp_id + "/invocations/output";
  var new_widget_url, attach_vol;

  console.log("Posting to experiment: " + object_path);
  /* Upload files. */
  if (file_count > 0) {
    var post_obj = obj_locate(experiment_path);

    // Minimal error checking.
    if (post_obj === '')
      return;

    var new_experiment_url = upload_to_vol(experiment_path, post_obj, formdata);

    // Change posting urls as needed.
    experiment_path = occam_root + new_experiment_url;

    // Rebuild our own URL.
    var create_widget_url = new_experiment_url.replace(new RegExp("/worksets/", "gm"), "/create/");
    create_widget_url = create_widget_url.replace(new RegExp("experiments/", "gm"), "");
    create_widget_url = str_root() + create_widget_url;

    // Get a new widget to tie output to.
    new_widget_url = request_new_widget(create_widget_url);
  }
  else {
    // No files uploaded, no new object created - reuse the current widget.
    console.log("No files added. Skipping upload.");
    new_widget_url = window.location.href.split("?")[0];
    console.log("Setting url to: " + new_widget_url);
  }

  /* Queue a job. */
  queue_job(experiment_path);
  poll_loop(experiment_path, register_url, new_widget_url, file_count);
}

// Event Handler Functions //////////////////////

// Triggers an experiment run.
function button_go() {
  plot_count = 0;
  result_iframe = 2;
  spin_start();
  run_experiment();
}

// Move from the input screen to the first plot.
function result_button() {
  flip_card_1();
  flip_card_2();
  setTimeout(toggle_card_1,400);
  setTimeout(toggle_card_2,400);
}

// Move from any plot back to the input screen.
function input_button() {
  flip_card_2();
  flip_card_1();
  setTimeout(toggle_card_1,400);
  setTimeout(toggle_card_2,400);
}

// Move to the next widget.
function move_to_next_widget() {
  window.location = "/widget/" + ((+cur_index() == +widget_count) ? "1" : (+cur_index() + 1));
}

// Move to the previous widget.
function move_to_prev_widget() {
  window.location =
    "/widget/" + ((cur_index() === "1") ? widget_count : (+cur_index() - 1));
}

// Delete the widget.
function del_button() {
  $.ajax({
    xhrFields: {
      withCredentials: true
    },
    url: '/widget/' + cur_index(),
    type: 'DELETE',
    crossDomain: true,
    success: function(data, textStatus, request) {
      if (data == 0)
        window.location = "/";
      else
        window.location = '/widget/' + data;
    },
    error: function(data) {
      console.log("Failed to delete widget.")
    }
  });
}

function rename_button() {
  window.location = "/name/" + cur_index();
}

function advanced_button() {
  window.location = "/origin/" + cur_index();
}

function index_button() {
  window.location = "/";
}

// Initialization.
window.onload = function() {
  var ib          = document.getElementById('indexbtn'),
      rnb         = document.getElementById('renamebtn'),
      dlb         = document.getElementById('delbtn'),
      adv         = document.getElementById('advancedbtn'),
      nw          = document.getElementById('nextWidget'),
      pw          = document.getElementById('prevWidget');
      td1         = document.getElementById('prog1'),
      td2         = document.getElementById('prog2'),
      td3         = document.getElementById('prog3'),
      td4         = document.getElementById('input1'),
      td5         = document.getElementById('input2'),
      td6         = document.getElementById('input3'),
      go          = document.getElementById('RunMe'),
      Results     = document.getElementById('flipresults'),
      Inputs1     = document.getElementById('inputsB1'),
      Inputs2     = document.getElementById('inputsB2'),
      nextresult1 = document.getElementById('nextR1'),
      nextresult2 = document.getElementById('nextR2');

  plot_count   = document.body.getAttribute("plot_count");
  widget_count = document.body.getAttribute("widget_count");
  obj_pre      = document.body.getAttribute("obj_pre");
  exp_id       = document.body.getAttribute("exp_id");
  occam_root   = 'https://' + document.body.getAttribute("OCCAM_ADDR");

  display_result();

  // Smoke test, don't continue if key elements are missing.
  if (!td1)
    return;

  // Handle dragover.
  td1.addEventListener('dragover', function(e) {drag_over(e)});
  td2.addEventListener('dragover', function(e) {drag_over(e)});
  td3.addEventListener('dragover', function(e) {drag_over(e)});
  td4.addEventListener('dragover', function(e) {drag_over(e)});
  td5.addEventListener('dragover', function(e) {drag_over(e)});
  td6.addEventListener('dragover', function(e) {drag_over(e)});

  // Get file data on drop
  // Load all of the files the user drags in.
  td1.addEventListener('drop', function(e) {get_data(e, 'prog1')});
  td2.addEventListener('drop', function(e) {get_data(e, 'prog2')});
  td3.addEventListener('drop', function(e) {get_data(e, 'prog3')});
  td4.addEventListener('drop', function(e) {get_data(e, 'input1')});
  td5.addEventListener('drop', function(e) {get_data(e, 'input2')});
  td6.addEventListener('drop', function(e) {get_data(e, 'input3')});

  // Handle various click events.
  go.addEventListener('click', button_go);
  Results.addEventListener('click', result_button);
  Inputs1.addEventListener('click', input_button);
  Inputs2.addEventListener('click', input_button);
  nextresult1.addEventListener('click', next_result);
  nextresult2.addEventListener('click', next_result);

  // Widget transition buttons. Disable if we are in the min view.
  if (document.body.getAttribute("is_min") === "true")
    return;

  pw.addEventListener('click', move_to_prev_widget);
  nw.addEventListener('click', move_to_next_widget);
  ib.addEventListener('click', index_button);
  rnb.addEventListener('click', rename_button);
  dlb.addEventListener('click', del_button);
  adv.addEventListener('click', advanced_button);

  // Autoload the first plot if we are coming from another widget.
  if (document.body.getAttribute("start_back") === "true") {
    result_button();
  }
}
