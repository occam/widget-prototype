#!/usr/bin/env ruby
require 'rubygems'
require 'sinatra'
require 'active_record'
require 'pg'
require './models/experiment.rb'
require './models/output.rb'
require './init'
require 'open-uri'

configure :development do
  set :OCCAM_ADDR,  "10.211.55.7:9292"
  ActiveRecord::Base.establish_connection(
    :adapter => "postgresql",
    :host    => "localhost",
    :username => "widgetman",
    :password => "widget",
    :database => "widget"
  )
end
configure :production do
  set :OCCAM_ADDR, "10.211.55.7:9292"
  ActiveRecord::Base.establish_connection(
    :adapter => "postgresql",
    :host    => "localhost",
    :username => "widgetman",
    :password => "widget",
    :database => "widget"
  )
end

def widget_count()
  return Experiment.all().count
end

# Handle stylesheets.
get '/stylesheets/:name.css' do
  content_type 'text/css', :charset => 'utf-8'
  scss (:"public/stylesheets/sass/#{params[:name]}")
end

# Index.
get '/' do
  @objects = Experiment.all()
  haml :index
end

# Access a widget.
get '/widget/:id' do
  headers 'Access-Control-Allow-Origin' => '*'

  if params[:id].to_i == 0
    status 404
    return
  end

  @object      = Experiment.find(params[:id])
  @widgetcount = Experiment.all().count
  @plotcount   = Output.where(oobject_id: params[:id]).count

  @obj_pre = "https://#{settings.OCCAM_ADDR}/worksets/"\
    "#{@object.workset_uuid}/#{@object.workset_revision}/objects/"

  haml :show
end

# Preflight.
options '/widget/:id' do
  headers 'Access-Control-Allow-Origin' => '*',
    'Access-Control-Allow-Methods' => 'GET, DELETE'
end

# Deletes the given widget.
delete '/widget/:id' do
  Output.where(oobject_id: params[:id]).destroy_all
  delete_widget(Experiment.find(params[:id]))

  status 200
  if Experiment.all().count < params[:id].to_i
    "#{params[:id].to_i - 1}"
  else
    "#{params[:id]}"
  end
end

# Redirect to widget's origin.
get '/origin/:id' do
  headers 'Access-Control-Allow-Origin' => '*'

  if params[:id].to_i == 0
    status 404
    return
  end

  @object = Experiment.find(params[:id])
  @obj_pre = "https://#{settings.OCCAM_ADDR}/worksets/"\
    "#{@object.workset_uuid}/#{@object.workset_revision}/objects/"

  redirect "https://#{settings.OCCAM_ADDR}/worksets/"\
    "#{@object.workset_uuid}/#{@object.workset_revision}/experiments/"\
    "#{@object.experiment_uuid}/#{@object.experiment_revision}"
end

# Show results for the given widget.
get '/results/:id' do
  headers 'Access-Control-Allow-Origin' => '*'

  if params[:id].to_i == 0
    status 404
    return
  end

  @object = Experiment.find(params[:id])
  redirect  "/results/#{params[:id]}/output/1"
end

# Create a widget based on the occam object specified by workset uuid, workset
# revision, exp. uuid and exp. revision. And redirect.
get '/export/:w_uuid/:w_rev/:ex_uuid/:ex_rev' do
  count = Experiment.all().count
  o = Experiment.new id:    count + 1,
    workset_uuid:        params[:w_uuid],
    workset_revision:    params[:w_rev],
    experiment_uuid:     params[:ex_uuid],
    experiment_revision: params[:ex_rev]
  o.save
  redirect "/name/#{o.id}"
end

# Form for renaming widget.
get '/name/:id' do

  if params[:id].to_i == 0
    status 404
    return
  end

  @id = params[:id]
  @current_widgets = Experiment.all()
  haml :namewidget
end

# Deletes the given widget and maintains all of the IDs.
def delete_widget(o)
  # The old widget is dead, long live the new widget.
  old_id = o.id
  o.destroy

  # Update IDs so rotation remains easy.
  Experiment.where("id > ?", old_id).each do |u|
    # Delete all plots of the widget being overwritten.
    Output.where(oobject_id: u.id - 1).destroy_all
    Output.where(oobject_id: u.id).update_all(oobject_id: u.id - 1)

    # Migrate all plots from the original position.
    u.id = u.id - 1
    u.save
  end
end

# Handles posts for renamings.
post '/name/:id' do

  if params[:id].to_i == 0
    status 404
    return
  end

  o = Experiment.find(params[:id])
  if not o
    status 404
    return
  end

  # Add extra checks if we are reusing a
  # name.
  if params.has_key?(:Reuse)
    r = Experiment.find_by friendly_name: params[:Reuse]
    if r
      # Clear overwritten plots.
      Output.where(oobject_id: r.id).destroy_all

      # Register the plots to the reassigned widget.
      Output.where(oobject_id: o.id).update_all(oobject_id: r.id)

      # Change the occam details to match the new information.
      r.workset_uuid        = o.workset_uuid
      r.workset_revision    = o.workset_revision
      r.experiment_uuid     = o.experiment_uuid
      r.experiment_revision = o.experiment_revision

      delete_widget(o)
      if params.has_key?(:backside)
        redirect "/widget/#{r.id}?backside"
      else
        redirect "/widget/#{r.id}"
      end
    end
  elsif params.has_key?(:Name)
    # Don't allow reuse of names.
    if Experiment.where(friendly_name: params[:Name]).count > 0
      @id              = params[:id]
      @current_widgets = Experiment.all()

      @warnmesg = "WARNING: Name already taken, try another name or"\
        " save over the existing widget."

      haml :namewidget
    else
      o.friendly_name = params[:Name]
      o.save
      if params.has_key?(:backside)
        redirect "/widget/#{params[:id]}?backside"
      else
        redirect "/widget/#{params[:id]}"
      end
    end
  else
    status 404
    return
  end
end

# Create a widget based on the occam object specified by workset uuid, workset
# revision, exp. uuid and exp. revision.
get '/create/:w_uuid/:w_rev/:ex_uuid/:ex_rev' do
  o = Experiment.new id:    widget_count() + 1,
    workset_uuid:        params[:w_uuid],
    workset_revision:    params[:w_rev],
    experiment_uuid:     params[:ex_uuid],
    experiment_revision: params[:ex_rev]
  o.save
  request.base_url + "/widget/#{o.id}"
end

# Clear registered plots for this widget.
post '/widget/:id/clear' do
  Output.where(oobject_id: params[:id]).destroy_all
end

# Register information about the object via parameters.
get '/widget/:id/register' do

  if params[:id].to_i == 0
    status 404
    return
  end

  count = Output.where(oobject_id: params[:id]).count
  o = Experiment.find(params[:id])
  if o
    # Register by widget/someid/register?object=e2f8a339d19359b1a9
    r = Output.new plotid: count+1,
      oobject_id:          params[:id],
      objectid:            params[:object]
    r.save
  end
end

# Show the widget's plots.
get '/results/:id/output/:outid' do

  if params[:id].to_i == 0 or params[:outid].to_i == 0
    status 404
    return
  end

  @output    = Output.where(oobject_id: params[:id], plotid: params[:outid])
  @plotcount = Output.where(oobject_id: params[:id]).count
  @object    = Experiment.find(params[:id])
  haml :result
end

# For debug purposes.
get '/widget/:id/outlist' do
  @output = Output.where(oobject_id: params[:id])
  @object = Experiment.find(params[:id])
  haml :outlist
end

# Basic compare. Currently used to test widget comparisons. Will be replaced
# with the "deck" concept.
get '/compare' do
  if not params[:a] or not params[:b] or
      params[:a].to_i > widget_count() or
      params[:b].to_i > widget_count()

    status 404
    return
  end

  haml :compare
end

# Redirect all to index.
get '/*' do
  redirect '/'
end

# Close the database connection on shutdown.
after do
  ActiveRecord::Base.connection.close
end
